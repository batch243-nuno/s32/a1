const http = require('http');
const port = 4000;

const server = http.createServer((request, response) => {
  // reponse welcome to booking system
  if (request.url == '/') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Welcome to Booking System');
  }

  // response welcome to profile page
  else if (request.url == '/profile' && request.method == 'GET') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Welcome to your profile!');
  }
  // response to courses page
  else if (request.url == '/courses' && request.method == 'GET') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Here’s our courses available');
  }
  //   response to addcourse page usign post method
  else if (request.url == '/addcourses' && request.method == 'POST') {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Add a course to our resources');
  } else {
    response.writeHead(404, { 'Content-Type': 'text/plain' });
    response.end('Page Note Found!');
  }
});
server.listen(port);

console.log(`Server running at localhost:${port}`);
